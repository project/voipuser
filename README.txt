
== Introduction ==

The VoIP User module provides VoIP Extensions to Drupal users.

The VoIP User module also comes with a simple directory page that lists all the VoIP Users in the system with their respective extension fields.  


== Installation ==

This module should be downloaded and enabled just like any other Drupal module.


== Configuration ==

This module provides VoIP extensions to users via Drupal Content Profiles (http://drupal.org/project/content_profile).  In order to so,

1. Go to /admin/content/types/list and either create or select the content type that will be used to provide VoIP extensions to users in the system
2. Open that content type for editing by clicking on /admin/content/node-type/<mytype>
3. Select "Content Profile"
4. Set "Use this content type as a content profile for users"
5. Add a VoIP Extension to this content type by following the instructions provided in the VoIP Extension module's README.txt file
6. Save

From now on, all the users that are eligible to that content profile will automatically have VoIP extensions associated with them.  To learn more about how to configure content profiles, check the documentation provided by the Content Profile module.


== Accessing the default VoIP User directory ==

1. Go to /admin/build/modules/ and enable the Views UI module
2. Go to /admin/build/views/list and enable the voipuser_directory view
3. Access the VoIP User directory by going to /voip/extension/user


== About ==

This module has been originally designed and implemented by the MIT Center for Civic Media (http://civic.mit.edu/) as part of the VoIP Drupal (http://drupal.org/project/voipdrupal/) initiative.

